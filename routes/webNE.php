<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('login');
// });
Route::get('/', function(){
    return view('layout.master');
});

Route::get('/a', 'HomeController@index')->name('login');
Route::get('/register', 'HomeController@index')->name('login');

Auth::routes();
Route::resource('posts', 'PostModelController');
Route::get('/home', 'HomeController@index')->name('home');

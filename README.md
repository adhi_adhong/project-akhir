<p align="center"><a href="https://www.sosmed.com" target="_blank"></a></p>

![sosmed1](public/img/logo1.png)

<p align="center">
<a href=""><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href=""><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href=""><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href=""><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

![sosmed](public/img/logo2.png)

![sosmedlogin](public/img/SimulasiSosmedRegister.gif)

![sosmedregister](public/img/SimulasiSosmedLogin.gif)

## Kelompok

<label>1. Nama : Maftuhin Arif</label><br>
<label>2. Nama : Adhi Suwartono</label>

## Tema Project

Project ini mengambil tema tentang aplikasi sosial media, yang bisa membuat posting, likes, comments, shares.

## Nama Project

Aplikasi <b>Sosmed</b>

## About Sosmed

Sosmed ini bisa digunakan untuk ngerumpi, saling bertukar status, gagasan, ide, komentar, opini atau apapun yang dengan sangat mudah melalui platform web dan mobile, dari siapapun di seluruh penjuru dunia.

Sosmed simple, powerfull, murah, cepat, ramai, dan dari mana saja kapan saja.

## Entity Relationship Diagram

![ERD](public/img/ERD.png)

## Link

Link Demo Aplikasi : http://sosmed-ngerumpi.herokuapp.com/login
<br>
Link Deploy : http://sosmed-ngerumpi.herokuapp.com/login

Aplikasi Sosmed ini open-source, dipersilahkan kepada siapapun untuk ikut mengembangkan aplikasi ini agar dapat menambah aplikasi sosial media yang ada di Indonesia.

@extends('layout.lay_profil')

@section('content')
    
  <!-- Content Wrapper. Contains page content -->
  <div class="content">
    <!-- Content Header (Page header) -->
    {{-- <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item active">User Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> --}}

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                    src="{{asset('admin/dist/img/user4-128x128.jpg')}}"
                       alt="User profile picture">
                </div>

                {{-- <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3> --}}

                <p class="text-muted text-center">Software Engineer</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Followers</b> <a class="float-right">1,322</a>
                  </li>
                  <li class="list-group-item">
                    <b>Following</b> <a class="float-right">543</a>
                  </li>
                  <li class="list-group-item">
                    <b>Friends</b> <a class="float-right">13,287</a>
                  </li>
                </ul>

                <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fas fa-book mr-1"></i> Education</strong>

                <p class="text-muted">
                  B.S. in Computer Science from the University of Tennessee at Knoxville
                </p>

                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

                <p class="text-muted">Malibu, California</p>

                <hr>

                <strong><i class="fas fa-pencil-alt mr-1"></i> Skills</strong>

                <p class="text-muted">
                  <span class="tag tag-danger">UI Design</span>
                  <span class="tag tag-success">Coding</span>
                  <span class="tag tag-info">Javascript</span>
                  <span class="tag tag-warning">PHP</span>
                  <span class="tag tag-primary">Node.js</span>
                </p>

                <hr>

                <strong><i class="far fa-file-alt mr-1"></i> Notes</strong>

                <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Activity</a></li>
                  <li class="nav-item"><a class="nav-link" href="#buatpost" data-toggle="tab">Posting Rumpi</a></li>
                  <li class="nav-item"><a class="nav-link" href="#search" data-toggle="tab">Search</a></li>
                  <li class="nav-item"><a class="nav-link" href="#following" data-toggle="tab">Following</a></li>
                  <li class="nav-item"><a class="nav-link" href="#follower" data-toggle="tab">Follower</a></li>
                  {{-- <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">timeline</a></li> --}}
                  {{-- <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">seting</a></li> --}}
                  
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">

                    <div class="active tab-pane" id="activity">
                    <!-- Daftar Post -->
                    @forelse ($posts as $key=>$value)
                    <!-- Post -->
                    <div class="post">
                        <div class="user-block">
                        <img class="img-circle img-bordered-sm" src="{{asset('admin/dist/img/user1-128x128.jpg')}}" alt="user image">
                        <span class="username">
                            <a href="#">Adhi Suwartono</a>
                            {{-- {{ Auth::user()->name }} --}}
                            <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                        </span>
                        <span class="description">Shared publicly - 7:30 PM today</span>
                        </div>
                        <!-- /.user-block -->
                        {{-- <img class="img-circle img-bordered-sm" src="{{$value->thumbnail}}" alt="Posts Thumbnail"> --}}
                        <p>
                        {{$value->konten}}
                        </p>

                        <p>
                        <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a>
                        <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
                        <span class="float-right">
                            <a href="#" class="link-black text-sm">
                            <i class="far fa-comments mr-1"></i> Comments (5)
                            </a>
                        </span>
                        </p>

                        <input class="form-control form-control-sm" type="text" placeholder="Type a comment">
                    </div>
                    <!-- /.post -->
                    @empty
                    <p>You have no post</p>  
                    @endforelse

                    <!-- Post -->
                    <div class="post clearfix">
                        <div class="user-block">
                        <img class="img-circle img-bordered-sm" src="{{asset('admin/dist/img/user7-128x128.jpg')}}" alt="User Image">
                        <span class="username">
                            <a href="#">Sarah Ross</a>
                            <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                        </span>
                        <span class="description">Sent you a message - 3 days ago</span>
                        </div>
                        <!-- /.user-block -->
                        <p>
                        Lorem ipsum represents a long-held tradition for designers,
                        typographers and the like. Some people hate it and argue for
                        its demise, but others ignore the hate as they create awesome
                        tools to help create filler text for everyone from bacon lovers
                        to Charlie Sheen fans.
                        </p>

                        <form class="form-horizontal">
                        <div class="input-group input-group-sm mb-0">
                            <input class="form-control form-control-sm" placeholder="Response">
                            <div class="input-group-append">
                            <button type="submit" class="btn btn-danger">Send</button>
                            </div>
                        </div>
                        </form>
                    </div>
                    <!-- /.post -->

                    <!-- Post -->
                    <div class="post">
                        <div class="user-block">
                        <img class="img-circle img-bordered-sm" src="{{asset('admin/dist/img/user6-128x128.jpg')}}" alt="User Image">
                        <span class="username">
                            <a href="#">Adam Jones</a>
                            <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                        </span>
                        <span class="description">Posted 5 photos - 5 days ago</span>
                        </div>
                        <!-- /.user-block -->
                        <div class="row mb-3">
                        <div class="col-sm-6">
                            <img class="img-fluid" src="{{asset('admin/dist/img/photo1.png')}}" alt="Photo">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <div class="row">
                            <div class="col-sm-6">
                                <img class="img-fluid mb-3" src="{{asset('admin/dist/img/photo2.png')}}" alt="Photo">
                                <img class="img-fluid" src="{{asset('admin/dist/img/photo3.jpg')}}" alt="Photo">
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <img class="img-fluid mb-3" src="{{asset('admin/dist/img/photo4.jpg')}}" alt="Photo">
                                <img class="img-fluid" src="{{asset('admin/dist/img/photo1.png')}}" alt="Photo">
                            </div>
                            <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <p>
                        <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a>
                        <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
                        <span class="float-right">
                            <a href="#" class="link-black text-sm">
                            <i class="far fa-comments mr-1"></i> Comments (5)
                            </a>
                        </span>
                        </p>

                        <input class="form-control form-control-sm" type="text" placeholder="Type a comment">
                    </div>
                    <!-- /.post -->
                <!-- End Daftar Post -->
                    </div>

                        
                      <!-- /.tab-pane -->
                  <div class="tab-pane" id="follower">
                    <!-- The timeline -->
                    <div class="timeline timeline-inverse">
                     
                      <!-- timeline item -->
                      <div>
                        <i class="fas fa-user bg-info"></i>

                        <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> 5 mins ago</span>
                          <h3 class="timeline-header border-0"><a href="#">Praven Jordan</a> sudah mengikuti Anda.
                          </h3>
                          <div class="timeline-footer">
                            <a href="profil" class="btn btn-primary btn-sm">Lihat Profil</a>
                            <a href="/posts/" class="btn btn-danger btn-sm">block</a>
                          </div>
                        </div>
                      </div>
                      <!-- END timeline item -->
                      <!-- timeline item -->
                      <div>
                        <i class="fas fa-user bg-info"></i>

                        <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> 5 mins ago</span>

                          <h3 class="timeline-header border-0"><a href="#">Kevin Sanjaya</a> sudah mengikuti Anda.
                          </h3>
                          <div class="timeline-footer">
                            <a href="profil" class="btn btn-primary btn-sm">Lihat Profil</a>
                            <a href="/posts/" class="btn btn-danger btn-sm">block</a>
                          </div>
                        </div>
                      </div>
                      <!-- END timeline item -->
                      <!-- timeline item -->
                      <div>
                        <i class="fas fa-user bg-info"></i>

                        <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> 5 mins ago</span>

                          <h3 class="timeline-header border-0"><a href="#">Bruse Lee</a> sudah mengikuti Anda.
                          </h3>
                          <div class="timeline-footer">
                            <a href="profil" class="btn btn-primary btn-sm">Lihat Profil</a>
                            <a href="/posts/" class="btn btn-danger btn-sm">block</a>
                          </div>
                        </div>
                      </div>
                      <!-- END timeline item -->
                      <!-- timeline item -->
                      <div>
                        <i class="fas fa-user bg-info"></i>

                        <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> 5 mins ago</span>

                          <h3 class="timeline-header border-0"><a href="#">Sarah Young</a> sudah mengikuti Anda.
                          </h3>
                          <div class="timeline-footer">
                            <a href="profil" class="btn btn-primary btn-sm">Lihat Profil</a>
                            <a href="/posts/" class="btn btn-danger btn-sm">block</a>
                          </div>
                        </div>
                      </div>
                      <!-- END timeline item -->
                      
                      <!-- timeline item -->
                      <div>
                        <i class="fas fa-camera bg-purple"></i>

                        <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> 2 days ago</span>

                          <h3 class="timeline-header"><a href="#">Mina Lee</a> sudah mengikuti Anda</h3>

                          <div class="timeline-body">
                            <div class="timeline-footer">
                            <a href="profil" class="btn btn-primary btn-sm">Lihat Profil</a>
                            <a href="/posts/" class="btn btn-danger btn-sm">block</a>
                          </div>
                          </div>
                        </div>
                      </div>
                      <!-- END timeline item -->
                      <div>
                        <i class="far fa-clock bg-gray"></i>
                      </div>
                    </div>
                  </div>
                  <!-- /.tab-pane -->

                  
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="following">
                    <!-- The timeline -->
                    <div class="timeline timeline-inverse">
                     
                      <!-- timeline item -->
                      <div>
                        <i class="fas fa-user bg-info"></i>

                        <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> 5 mins ago</span>
                          <h3 class="timeline-header border-0"><a href="#">Praven Jordan</a> sudah Anda follow.
                          </h3>
                          <div class="timeline-footer">
                            <a href="profil" class="btn btn-primary btn-sm">Lihat Profil</a>
                            <a href="/posts/" class="btn btn-danger btn-sm">unfollow</a>
                          </div>
                        </div>
                      </div>
                      <!-- END timeline item -->
                      <!-- timeline item -->
                      <div>
                        <i class="fas fa-user bg-info"></i>

                        <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> 5 mins ago</span>

                          <h3 class="timeline-header border-0"><a href="#">Kevin Sanjaya</a> sudah Anda follow.
                          </h3>
                          <div class="timeline-footer">
                            <a href="profil" class="btn btn-primary btn-sm">Lihat Profil</a>
                            <a href="/posts/" class="btn btn-danger btn-sm">unfollow</a>
                          </div>
                        </div>
                      </div>
                      <!-- END timeline item -->
                      <!-- timeline item -->
                      <div>
                        <i class="fas fa-user bg-info"></i>

                        <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> 5 mins ago</span>

                          <h3 class="timeline-header border-0"><a href="#">Bruse Lee</a> sudah Anda follow.
                          </h3>
                          <div class="timeline-footer">
                            <a href="profil" class="btn btn-primary btn-sm">Lihat Profil</a>
                            <a href="/posts/" class="btn btn-danger btn-sm">unfollow</a>
                          </div>
                        </div>
                      </div>
                      <!-- END timeline item -->
                      <!-- timeline item -->
                      <div>
                        <i class="fas fa-user bg-info"></i>

                        <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> 5 mins ago</span>

                          <h3 class="timeline-header border-0"><a href="#">Sarah Young</a> sudah Anda follow.
                          </h3>
                          <div class="timeline-footer">
                            <a href="profil" class="btn btn-primary btn-sm">Lihat Profil</a>
                            <a href="/posts/" class="btn btn-danger btn-sm">unfollow</a>
                          </div>
                        </div>
                      </div>
                      <!-- END timeline item -->
                      <!-- timeline item -->
                      {{-- <div>
                        <i class="fas fa-comments bg-warning"></i>

                        <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> 27 mins ago</span>

                          <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>

                          <div class="timeline-body">
                            Take me to your leader!
                            Switzerland is small and neutral!
                            We are more like Germany, ambitious and misunderstood!
                          </div>
                          <div class="timeline-footer">
                            <a href="#" class="btn btn-warning btn-flat btn-sm">View comment</a>
                          </div>
                        </div>
                      </div> --}}
                      <!-- END timeline item -->
                      <!-- timeline time label -->
                      {{-- <div class="time-label">
                        <span class="bg-success">
                          3 Jan. 2014
                        </span>
                      </div> --}}
                      <!-- /.timeline-label -->
                      <!-- timeline item -->
                      <div>
                        <i class="fas fa-camera bg-purple"></i>

                        <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> 2 days ago</span>

                          <h3 class="timeline-header"><a href="#">Mina Lee</a> sudah Anda Follow</h3>

                          <div class="timeline-body">
                            <div class="timeline-footer">
                            <a href="profil" class="btn btn-primary btn-sm">Lihat Profil</a>
                            <a href="/posts/" class="btn btn-danger btn-sm">unfollow</a>
                          </div>
                          </div>
                        </div>
                      </div>
                      <!-- END timeline item -->
                      <div>
                        <i class="far fa-clock bg-gray"></i>
                      </div>
                    </div>
                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="buatpost">
                    <form class="form-horizontal" action="/posts" method="POST">
                    @csrf
                      <div class="form-group row">
                        <label for="inputPost" class="col-sm-2 col-form-label">Posting Rumpi</label>
                        <div class="col-sm-10">
                          <textarea name="konten" class="form-control" id="inputPost" placeholder="Mau ngerumpi apa ?"></textarea>
                        </div>
                      </div>
                      
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-primary">Kirim Rumpi</button>
                        </div>
                      </div>
                    </form>
                  </div>
                    <div class="tab-pane" id="search">
                        <form class="form-horizontal" action="/posts" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label for="inputPost" class="col-sm-2 col-form-label">Cari Akun</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="cariakun" placeholder="cari nama akun">
                            
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Cari</button>
                            </div>
                        </div>
                        </form>
                    </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
    <!-- Control Sidebar -->
  {{-- <aside class="control-sidebar control-sidebar-dark"> --}}
    <!-- Control sidebar content goes here -->
  {{-- </aside> --}}
  <!-- /.control-sidebar -->
{{-- </div> --}}
<!-- ./wrapper -->


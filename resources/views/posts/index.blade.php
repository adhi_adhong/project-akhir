<a href="/posts/create" class="btn btn-primary mb-3">Buat Post</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Konten</th>
                <th scope="col">Thumbnail</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($posts as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->konten}}</td>
                        <td>{{$value->thumbnail}}</td>
                        <td>
                            <a href="/posts/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/posts/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/posts/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>Belum ada post</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

     
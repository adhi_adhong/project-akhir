<div>
        <h2>Edit Post {{$posts->id}}</h2>
        <form action="/posts/{{$posts->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="konten">Konten</label>
                <input type="text" class="form-control" name="konten" value="{{$posts->title}}" id="konten" placeholder="Masukkan Konten">
                @error('konten')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="thumbnail">Thumbnail</label>
                <input type="text" class="form-control" name="thumbnail"  value="{{$posts->body}}"  id="thumbnail" placeholder="Masukkan Thumbnail">
                @error('thumbnail')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
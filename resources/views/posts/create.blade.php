<h2>Buat Post</h2>
        <form action="/posts" method="POST">
            @csrf
            <div class="form-group">
                <label for="konten">Konten</label>
                <input type="text" class="form-control" name="konten" id="konten" placeholder="Masukkan konten">
                @error('konten')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="thumbnail">Thumbnail</label>
                <input type="text" class="form-control" name="thumbnail" id="thumbnail" placeholder="Masukkan Thumbnail">
                @error('thumbnail')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>